curl -v --location --request POST 'http://apicurio:8080/api/artifacts' \
  --header 'Content-Type: application/json; artifactType=AVRO' \
  --header 'X-Registry-ArtifactId: urbano-value' \
  --data "@extract-service/src/main/avro/Urbano.avsc"

curl --location --request POST 'http://apicurio:8080/api/artifacts' \
  --header 'Content-Type: application/json; artifactType=AVRO' \
  --header 'X-Registry-ArtifactId: interurbano-value' \
  --data "@extract-service/src/main/avro/InterUrbano.avsc"

curl --location --request POST 'http://apicurio:8080/api/artifacts' \
  --header 'Content-Type: application/json; artifactType=AVRO' \
  --header 'X-Registry-ArtifactId: nivelservicio-value' \
  --data "@enrich-service/src/main/avro/NivelServicio.avsc"
