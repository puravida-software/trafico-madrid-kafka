package extract.service;

import io.micronaut.configuration.kafka.annotation.KafkaClient;
import io.micronaut.configuration.kafka.annotation.Topic;
import opendata.madrid.RecordInterUrbano;
import opendata.madrid.RecordUrbano;

import java.util.List;

@KafkaClient(id="trafico", batch = true)
public interface TraficoProducer {

    @Topic("urbano")
    void sendStatusUrbano(List<RecordUrbano> record);

    @Topic("interurbano")
    void sendStatusInterUrbano(List<RecordInterUrbano> record);

}
