package extract.service.xml;

import opendata.madrid.RecordInterUrbano;
import opendata.madrid.RecordUrbano;

import javax.xml.bind.annotation.XmlElement;
import java.util.Optional;

public class Pm {

    String idelem;
    String descripcion;
    Integer accesoAsociado;
    Integer intensidad;
    Integer ocupacion;
    Integer carga;
    Integer nivelServicio;
    Integer intensidadSat;
    String error;
    Integer velocidad;
    Integer subarea;
    String st_x;
    String st_y;

    public String getIdelem() {
        return idelem;
    }

    @XmlElement
    public void setIdelem(String idelem) {
        this.idelem = idelem;
    }

    public String getDescripcion() {
        return descripcion;
    }

    @XmlElement
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getAccesoAsociado() {
        return accesoAsociado;
    }

    @XmlElement
    public void setAccesoAsociado(Integer accesoAsociado) {
        this.accesoAsociado = accesoAsociado;
    }

    public Integer getIntensidad() {
        return intensidad;
    }

    @XmlElement
    public void setIntensidad(Integer intensidad) {
        this.intensidad = intensidad;
    }

    public Integer getOcupacion() {
        return ocupacion;
    }

    @XmlElement
    public void setOcupacion(Integer ocupacion) {
        this.ocupacion = ocupacion;
    }

    public Integer getCarga() {
        return carga;
    }

    @XmlElement
    public void setCarga(Integer carga) {
        this.carga = carga;
    }

    public Integer getNivelServicio() {
        return nivelServicio;
    }

    @XmlElement
    public void setNivelServicio(Integer nivelServicio) {
        this.nivelServicio = nivelServicio;
    }

    public Integer getIntensidadSat() {
        return intensidadSat;
    }

    @XmlElement
    public void setIntensidadSat(Integer intensidadSat) {
        this.intensidadSat = intensidadSat;
    }

    public String getError() {
        return error;
    }

    @XmlElement
    public void setError(String error) {
        this.error = error;
    }

    public Integer getVelocidad() {
        return velocidad;
    }

    @XmlElement
    public void setVelocidad(Integer velocidad) {
        this.velocidad = velocidad;
    }

    public Integer getSubarea() {
        return subarea;
    }

    @XmlElement
    public void setSubarea(Integer subarea) {
        this.subarea = subarea;
    }

    public String getSt_x() {
        return st_x;
    }

    @XmlElement
    public void setSt_x(String st_x) {
        this.st_x = st_x;
    }

    public String getSt_y() {
        return st_y;
    }

    @XmlElement
    public void setSt_y(String st_y) {
        this.st_y = st_y;
    }


    public RecordUrbano asRecordUrbano(){
        RecordUrbano ret = new RecordUrbano();
        ret.setIdelem(getIdelem());
        ret.setIntensidad(Optional.ofNullable(getIntensidad()).orElse(0));
        ret.setOcupacion(Optional.ofNullable(getOcupacion()).orElse(0));
        ret.setCarga(Optional.ofNullable(getCarga()).orElse(0));
        ret.setNivelServicio(Optional.ofNullable(getNivelServicio()).orElse(0));
        ret.setIntensidadSat(Optional.ofNullable(getIntensidadSat()).orElse(0));
        ret.setError(Optional.ofNullable(getError()).orElse("N")=="S");
        return  ret;
    }

    public RecordInterUrbano asRecordInterUrbano(){
        RecordInterUrbano ret = new RecordInterUrbano();
        ret.setIdelem(getIdelem());
        ret.setIntensidad(Optional.ofNullable(getIntensidad()).orElse(0));
        ret.setOcupacion(Optional.ofNullable(getOcupacion()).orElse(0));
        ret.setCarga(Optional.ofNullable(getCarga()).orElse(0));
        ret.setNivelServicio(Optional.ofNullable(getNivelServicio()).orElse(0));
        ret.setVelocidad(Optional.ofNullable(getVelocidad()).orElse(0));
        ret.setError(Optional.ofNullable(getError()).orElse("N")=="S");
        return  ret;
    }
}
