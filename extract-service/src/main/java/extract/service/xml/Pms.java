package extract.service.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class Pms {

    String fecha_hora;

    List<Pm> pm;

    public String getFecha_hora() {
        return fecha_hora;
    }

    @XmlElement
    public void setFecha_hora(String fecha_hora) {
        this.fecha_hora = fecha_hora;
    }

    public List<Pm> getPm() {
        return pm;
    }

    @XmlElement
    public void setPm(List<Pm> pm) {
        this.pm = pm;
    }
}
