package extract.service;

import extract.service.xml.Pm;
import extract.service.xml.Pms;
import io.micronaut.scheduling.annotation.Scheduled;
import opendata.madrid.RecordInterUrbano;
import opendata.madrid.RecordUrbano;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class ExtractJob {

    @Scheduled(fixedDelay = "5m", initialDelay = "5s")
    void executeEveryFiveMinutes() {
        execute();
    }

    @Inject
    TraficoProducer traficoProducer;

    void execute(){
        String url = "https://informo.madrid.es/informo/tmadrid/pm.xml";
        try {
            System.out.println("start "+new Date());
            long ini = System.nanoTime();
            JAXBContext jaxbContext = JAXBContext.newInstance(Pms.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Pms pms = (Pms)jaxbUnmarshaller.unmarshal(new URL(url));

            List<RecordUrbano> urbanos = pms.getPm().stream().filter(p->p.getVelocidad()==null )
                    .map(Pm::asRecordUrbano).collect(Collectors.toList());

            List<RecordInterUrbano> interurbanos = pms.getPm().stream().filter(p->p.getVelocidad()!=null )
                    .map(Pm::asRecordInterUrbano).collect(Collectors.toList());

            traficoProducer.sendStatusUrbano(urbanos);

            traficoProducer.sendStatusInterUrbano(interurbanos);

            long end = System.nanoTime();
            System.out.println("Sent in "+((end-ini)/ 1_000_000_000.0)+" seg");
            System.out.println("end "+new Date());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
