package enrich.service;

import enrich.service.data.*;
import io.micronaut.configuration.kafka.annotation.KafkaListener;
import io.micronaut.configuration.kafka.annotation.OffsetReset;
import io.micronaut.configuration.kafka.annotation.OffsetStrategy;
import io.micronaut.configuration.kafka.annotation.Topic;
import io.reactivex.Flowable;
import opendata.madrid.NivelServicio;
import opendata.madrid.RecordInterUrbano;
import opendata.madrid.RecordUrbano;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@KafkaListener(offsetReset = OffsetReset.EARLIEST, batch = true)
public class StatusListener {

    @Inject
    InterUrbanoRepository interUrbanoRepository;

    @Inject
    SituacionProducer producer;

    @Topic("interurbano")
    public void receiveInterUrbano(List<RecordInterUrbano> beans){
        System.out.println("process "+beans.size()+" interurbanos");
        List<NivelServicio> status = new ArrayList<>();
        beans.stream().forEach((bean)->{
            InterUrbanoEntity entity;
            Optional<InterUrbanoEntity> optional = interUrbanoRepository.findById(bean.getIdelem());
            if (optional.isPresent()) {
                entity = optional.get();
                interUrbanoRepository.update(entity.rotate(bean));
            } else {
                entity = InterUrbanoEntity.fromBean(bean);
                interUrbanoRepository.save(entity.rotate(bean));
            }

            if (entity.getWhen4() != null) {
                status.add(buildStatus(entity));
            }
        });

        sendStatus(status);
    }

    @Inject
    UrbanoRepository urbanoRepository;

    @Topic("urbano")
    public void receiveUrbano( List<RecordUrbano> beans){
        System.out.println("process "+beans.size()+" urbanos");
        List<NivelServicio> status = new ArrayList<>();

        beans.stream().forEach((bean)->{
            UrbanoEntity entity;
            Optional<UrbanoEntity> optional = urbanoRepository.findById(bean.getIdelem());
            if( optional.isPresent() ) {
                entity = optional.get();
                urbanoRepository.update(entity.rotate(bean));
            }else {
                entity = UrbanoEntity.fromBean(bean);
                urbanoRepository.save(entity.rotate(bean));
            }

            if( entity.getWhen4() != null) {
                status.add(buildStatus(entity));
            }
        });

        sendStatus(status);
    }

    void sendStatus( List<NivelServicio> list){
        Flowable<NivelServicio> flowable = Flowable.fromIterable(list);
        producer.sendStatus(flowable).subscribe( item->{

        }, error->{
            System.out.println("Error sending status "+error);
        });
    }

    NivelServicio buildStatus(BaseEntity entity){
        NivelServicio nivelServicio = new NivelServicio();
        nivelServicio.setIdelem(entity.getId());

        nivelServicio.setWhen1(entity.getWhen1().getNano());
        nivelServicio.setNivelServicio1(entity.getNivelServicio1());

        nivelServicio.setWhen2(entity.getWhen2().getNano());
        nivelServicio.setNivelServicio2(entity.getNivelServicio2());

        nivelServicio.setWhen3(entity.getWhen3().getNano());
        nivelServicio.setNivelServicio3(entity.getNivelServicio3());

        nivelServicio.setWhen4(entity.getWhen4().getNano());
        nivelServicio.setNivelServicio4(entity.getNivelServicio4());

        return nivelServicio;
    }
}
