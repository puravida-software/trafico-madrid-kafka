package enrich.service.data;

import io.micronaut.data.jdbc.annotation.JdbcRepository;
import io.micronaut.data.model.query.builder.sql.Dialect;
import io.micronaut.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.Optional;

@JdbcRepository(dialect = Dialect.POSTGRES)
public abstract class InterUrbanoRepository implements CrudRepository<InterUrbanoEntity, String> {

    public abstract Optional<InterUrbanoEntity>findById(String id);

    @Transactional
    public abstract InterUrbanoEntity save(InterUrbanoEntity entity);

    @Transactional
    public abstract InterUrbanoEntity update(InterUrbanoEntity entity);
}
