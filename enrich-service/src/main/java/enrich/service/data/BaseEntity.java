package enrich.service.data;

import java.time.Instant;

public interface BaseEntity {

    String getId();

    Instant getWhen1();
    int getNivelServicio1();

    Instant getWhen2();
    int getNivelServicio2();

    Instant getWhen3();
    int getNivelServicio3();

    Instant getWhen4();
    int getNivelServicio4();

}
