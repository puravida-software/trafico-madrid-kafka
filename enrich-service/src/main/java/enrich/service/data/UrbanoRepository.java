package enrich.service.data;

import io.micronaut.data.jdbc.annotation.JdbcRepository;
import io.micronaut.data.model.query.builder.sql.Dialect;
import io.micronaut.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.Optional;

@JdbcRepository(dialect = Dialect.POSTGRES)
public abstract class UrbanoRepository implements CrudRepository<UrbanoEntity, String> {

    public abstract Optional<UrbanoEntity>findById(String id);

    @Transactional
    public abstract UrbanoEntity save(UrbanoEntity entity);

    @Transactional
    public abstract UrbanoEntity update(UrbanoEntity entity);
}
