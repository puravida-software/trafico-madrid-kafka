package enrich.service.data;

import io.micronaut.context.annotation.Provided;
import opendata.madrid.RecordUrbano;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;

@Entity
@Table(name="urbano_historico")
public class UrbanoEntity implements BaseEntity{

    @Id
    @Provided
    public String id;

    public Instant when1;
    public int intensidad1;
    public int ocupacion1;
    public int carga1;
    public int nivelServicio1;
    public boolean error1;
    public int intensidadSat1;

    public Instant when2;
    public int intensidad2;
    public int ocupacion2;
    public int carga2;
    public int nivelServicio2;
    public boolean error2;
    public int intensidadSat2;

    public Instant when3;
    public int intensidad3;
    public int ocupacion3;
    public int carga3;
    public int nivelServicio3;
    public boolean error3;
    public int intensidadSat3;

    public Instant when4;
    public int intensidad4;
    public int ocupacion4;
    public int carga4;
    public int nivelServicio4;
    public boolean error4;
    public int intensidadSat4;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Instant getWhen1() {
        return when1;
    }

    public void setWhen1(Instant when1) {
        this.when1 = when1;
    }

    public int getIntensidad1() {
        return intensidad1;
    }

    public void setIntensidad1(int intensidad1) {
        this.intensidad1 = intensidad1;
    }

    public int getOcupacion1() {
        return ocupacion1;
    }

    public void setOcupacion1(int ocupacion1) {
        this.ocupacion1 = ocupacion1;
    }

    public int getCarga1() {
        return carga1;
    }

    public void setCarga1(int carga1) {
        this.carga1 = carga1;
    }

    public int getNivelServicio1() {
        return nivelServicio1;
    }

    public void setNivelServicio1(int nivelServicio1) {
        this.nivelServicio1 = nivelServicio1;
    }

    public boolean isError1() {
        return error1;
    }

    public void setError1(boolean error1) {
        this.error1 = error1;
    }

    public int getIntensidadSat1() {
        return intensidadSat1;
    }

    public void setIntensidadSat1(int intensidadSat1) {
        this.intensidadSat1 = intensidadSat1;
    }

    public Instant getWhen2() {
        return when2;
    }

    public void setWhen2(Instant when2) {
        this.when2 = when2;
    }

    public int getIntensidad2() {
        return intensidad2;
    }

    public void setIntensidad2(int intensidad2) {
        this.intensidad2 = intensidad2;
    }

    public int getOcupacion2() {
        return ocupacion2;
    }

    public void setOcupacion2(int ocupacion2) {
        this.ocupacion2 = ocupacion2;
    }

    public int getCarga2() {
        return carga2;
    }

    public void setCarga2(int carga2) {
        this.carga2 = carga2;
    }

    public int getNivelServicio2() {
        return nivelServicio2;
    }

    public void setNivelServicio2(int nivelServicio2) {
        this.nivelServicio2 = nivelServicio2;
    }

    public boolean isError2() {
        return error2;
    }

    public void setError2(boolean error2) {
        this.error2 = error2;
    }

    public int getIntensidadSat2() {
        return intensidadSat2;
    }

    public void setIntensidadSat2(int intensidadSat2) {
        this.intensidadSat2 = intensidadSat2;
    }

    public Instant getWhen3() {
        return when3;
    }

    public void setWhen3(Instant when3) {
        this.when3 = when3;
    }

    public int getIntensidad3() {
        return intensidad3;
    }

    public void setIntensidad3(int intensidad3) {
        this.intensidad3 = intensidad3;
    }

    public int getOcupacion3() {
        return ocupacion3;
    }

    public void setOcupacion3(int ocupacion3) {
        this.ocupacion3 = ocupacion3;
    }

    public int getCarga3() {
        return carga3;
    }

    public void setCarga3(int carga3) {
        this.carga3 = carga3;
    }

    public int getNivelServicio3() {
        return nivelServicio3;
    }

    public void setNivelServicio3(int nivelServicio3) {
        this.nivelServicio3 = nivelServicio3;
    }

    public boolean isError3() {
        return error3;
    }

    public void setError3(boolean error3) {
        this.error3 = error3;
    }

    public int getIntensidadSat3() {
        return intensidadSat3;
    }

    public void setIntensidadSat3(int intensidadSat3) {
        this.intensidadSat3 = intensidadSat3;
    }

    public Instant getWhen4() {
        return when4;
    }

    public void setWhen4(Instant when4) {
        this.when4 = when4;
    }

    public int getIntensidad4() {
        return intensidad4;
    }

    public void setIntensidad4(int intensidad4) {
        this.intensidad4 = intensidad4;
    }

    public int getOcupacion4() {
        return ocupacion4;
    }

    public void setOcupacion4(int ocupacion4) {
        this.ocupacion4 = ocupacion4;
    }

    public int getCarga4() {
        return carga4;
    }

    public void setCarga4(int carga4) {
        this.carga4 = carga4;
    }

    public int getNivelServicio4() {
        return nivelServicio4;
    }

    public void setNivelServicio4(int nivelServicio4) {
        this.nivelServicio4 = nivelServicio4;
    }

    public boolean isError4() {
        return error4;
    }

    public void setError4(boolean error4) {
        this.error4 = error4;
    }

    public int getIntensidadSat4() {
        return intensidadSat4;
    }

    public void setIntensidadSat4(int intensidadSat4) {
        this.intensidadSat4 = intensidadSat4;
    }

    public static UrbanoEntity fromBean(RecordUrbano bean){
        UrbanoEntity ret = new UrbanoEntity();
        ret.id = bean.getIdelem();

        ret.when1 = Instant.now();
        ret.intensidad1 = bean.getIntensidad();
        ret.ocupacion1 = bean.getOcupacion();
        ret.carga1 = bean.getCarga();
        ret.nivelServicio1 = bean.getNivelServicio();
        ret.error1 = bean.getError();
        ret.intensidadSat1 = bean.getIntensidadSat();

        return ret;
    }

    public UrbanoEntity rotate(RecordUrbano bean){
        this.when4 = this.when3;
        this.intensidad4 = this.intensidad3;
        this.ocupacion4 = this.ocupacion3;
        this.carga4 = this.carga3;
        this.nivelServicio4 = this.nivelServicio3;
        this.error4 = this.error3;
        this.intensidadSat4 = this.intensidadSat3;

        this.when3 = this.when2;
        this.intensidad3 = this.intensidad2;
        this.ocupacion3 = this.ocupacion2;
        this.carga3 = this.carga2;
        this.nivelServicio3 = this.nivelServicio2;
        this.error3 = this.error2;
        this.intensidadSat3 = this.intensidadSat2;

        this.when2 = this.when1;
        this.intensidad2 = this.intensidad1;
        this.ocupacion2 = this.ocupacion1;
        this.carga2 = this.carga1;
        this.nivelServicio2 = this.nivelServicio1;
        this.error2 = this.error1;
        this.intensidadSat2 = this.intensidadSat1;

        this.when1 = Instant.now();
        this.intensidad1 = bean.getIntensidad();
        this.ocupacion1 = bean.getOcupacion();
        this.carga1 = bean.getCarga();
        this.nivelServicio1 = bean.getNivelServicio();
        this.error1 = bean.getError();
        this.intensidadSat1 = bean.getIntensidadSat();

        return this;
    }

}
