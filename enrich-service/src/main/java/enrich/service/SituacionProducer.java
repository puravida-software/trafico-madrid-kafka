package enrich.service;

import io.micronaut.configuration.kafka.annotation.KafkaClient;
import io.micronaut.configuration.kafka.annotation.Topic;
import io.reactivex.Flowable;
import opendata.madrid.NivelServicio;

@KafkaClient(id="niveles")
public interface SituacionProducer {

    @Topic("nivelservicio")
    Flowable<NivelServicio> sendStatus(Flowable<NivelServicio> flowable);


}
