package notify.service;

import io.micronaut.context.event.ApplicationEventListener;
import io.micronaut.context.event.StartupEvent;

import javax.inject.Singleton;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.LineNumberReader;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class EstacionesService implements ApplicationEventListener<StartupEvent> {

    Map<String, EstacionBean> estaciones = new HashMap<>();

    @Override
    public void onApplicationEvent(StartupEvent event) {
        try {
            String url = "https://datos.madrid.es/egob/catalogo/202468-104-intensidad-trafico.csv";

            File temp = File.createTempFile("estaciones",".csv");
            ReadableByteChannel readableByteChannel = Channels.newChannel(new URL(url).openStream());
            FileOutputStream fileOutputStream = new FileOutputStream(temp);
            FileChannel fileChannel = fileOutputStream.getChannel();
            fileOutputStream.getChannel()
                    .transferFrom(readableByteChannel, 0, Long.MAX_VALUE);

            LineNumberReader reader = new LineNumberReader(new FileReader(temp));
            String line;
            boolean first=true;
            while ((line = reader.readLine()) != null) {
                if (first) {
                    first=false;
                    continue;
                }
                String[] fields = line.split(";");
                EstacionBean add = new EstacionBean();
                add.setId(fields[2].replaceAll("\"", ""));
                add.setTipo_elem(fields[0].replaceAll("\"", ""));
                add.setDistrito(fields[1].replaceAll("\"", ""));
                add.setNombre(fields[4].replaceAll("\"", ""));
                add.setLatitud(fields[8].replaceAll("\"", ""));
                add.setLongitud(fields[7].replaceAll("\"", ""));
                estaciones.put(add.getId(), add);
            }
            System.out.println("Total estaciones leidas " + estaciones.size());
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }


}
