package notify.service;

import io.micronaut.configuration.kafka.annotation.KafkaListener;
import io.micronaut.configuration.kafka.annotation.OffsetReset;
import io.micronaut.configuration.kafka.annotation.OffsetStrategy;
import io.micronaut.configuration.kafka.annotation.Topic;
import opendata.madrid.NivelServicio;

import javax.inject.Inject;
import java.util.List;

@KafkaListener(offsetReset = OffsetReset.EARLIEST, offsetStrategy = OffsetStrategy.ASYNC, batch = true)
public class NivelServicioListener {

    @Inject
    EstacionesService estacionesService;

    @Topic("niveles")
    public void receiveInterUrbano(List<NivelServicio> beans){
        System.out.println("Chequeando niveles de servicio "+beans.size());
        beans.stream()
                .filter( bean -> estacionesService.estaciones.containsKey(bean.getIdelem()))
                .filter( bean -> {
                    return
                            bean.getNivelServicio1() >= 3 &&
                            bean.getNivelServicio2() >= 3;
                })
                .forEach( bean ->{
                    EstacionBean estacionBean = estacionesService.estaciones.get(bean.getIdelem());
                    System.out.println("Problemas serios en "+estacionBean.getNombre());
                });
    }

}
